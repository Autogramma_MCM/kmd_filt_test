﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ZedGraph;
using System.Collections;

namespace MD_Filt_Test
{

    public partial class Form1 : Form
    {
        Raw_data raw_Data;
        Conver conver;
        bool play;

        public void GUI_Raw_data_Update(Raw_data raw_Data)
        {
            if (raw_Data == null)
                return;

            if (comboBox1.Items.Count != raw_Data.data[0].Count())
            {
                comboBox1.Items.Clear();
                for (int i = 0; i < raw_Data.data[0].Count(); i++)
                    comboBox1.Items.Add(i);
                comboBox1.SelectedIndex = 0;
            }

            label1.Text = "Выборок: " + raw_Data.data.Count.ToString();
            label2.Text = "Позиция: " + raw_Data.pos.ToString();
            label3.Text = "Значение: " + raw_Data.data[raw_Data.pos][comboBox1.SelectedIndex].ToString();
            numericUpDown3.Maximum = raw_Data.data.Count();
            numericUpDown3.Value = raw_Data.pos;
            numericUpDown1.Maximum = conver.MaxLen;


        }
        
        public void GUI_Conver_data_Update(Conver conver)
        {
            if (conver == null)
                return;


            //считаем позиции вырезки графика
            int len = (int)numericUpDown1.Value;
            if (len > conver.Count)
                len = conver.Count;

            int endpos_lst = conver.Count - 1;
            int begpos_lst = conver.Count - len - 1;
            int endpos_prv = begpos_lst;
            int begpos_prv = conver.Count - (len * 2) - 1;

            if (begpos_lst < 0)
            {
                begpos_lst = 0;
                endpos_prv = 0;
                begpos_prv = 0;
            }
            if (begpos_prv < 0)
                begpos_prv = 0;

            //рисуем обший график
            {
                GraphPane plane = zedGraphControl1.GraphPane;

                plane.Title.Text = "Конвеер выборок 2000 позиций";
                plane.YAxis.Title.Text = "Напряжение";
                plane.XAxis.Title.Text = "Выброка";


                plane.CurveList.Clear();
                PointPairList pointPairs_old = new PointPairList();
                PointPairList pointPairs_red = new PointPairList();
                PointPairList pointPairs_blu = new PointPairList();

                for (int i = 0; i < begpos_prv; i++)
                    pointPairs_old.Add(i, conver.Item(i));

                for (int i = begpos_prv; i <= endpos_prv; i++)
                    pointPairs_red.Add(i, conver.Item(i));

                for (int i = begpos_lst; i <= endpos_lst; i++)
                    pointPairs_blu.Add(i, conver.Item(i));

                LineItem myCuve_old = plane.AddCurve("Осталной Конвеер", pointPairs_old, Color.Green, SymbolType.None);
                LineItem myCuve_red = plane.AddCurve("Окно из Конвеера", pointPairs_red, Color.Red, SymbolType.None);
                LineItem myCuve_blu = plane.AddCurve("Окно из Конвеера -1", pointPairs_blu, Color.Blue, SymbolType.None);

                plane.XAxis.Scale.Min = 0;
                plane.XAxis.Scale.Max = conver.Count;


                zedGraphControl1.AxisChange();
                zedGraphControl1.Invalidate();

                plane = zedGraphControl3.GraphPane;
                plane.Title.Text = "Наложение окон из конвеера в " + len.ToString() + " выборок";
                plane.YAxis.Title.Text = "Напряжение";
                plane.XAxis.Title.Text = "Выброка";

                plane.CurveList.Clear();
                PointPairList pointPairs_lst = new PointPairList();
                PointPairList pointPairs_prv = new PointPairList();

                

                for (int i = begpos_lst; i < endpos_lst; i++)
                {
                    pointPairs_lst.Add(i, conver.Item(i));
                }

                LineItem myCuve_b = plane.AddCurve("Окно из Конвеера", pointPairs_lst, Color.Blue, SymbolType.None);

                if (endpos_prv > 0)
                {
                    for (int i = begpos_prv; i < endpos_prv; i++)
                    {
                        pointPairs_prv.Add(i + len, conver.Item(i));
                    }

                    LineItem myCuve_r = plane.AddCurve("Окно из Конвеера -1", pointPairs_prv, Color.Red, SymbolType.None);
                }

                plane.XAxis.Scale.Min = begpos_lst;
                plane.XAxis.Scale.Max = endpos_lst;


                zedGraphControl3.AxisChange();
                zedGraphControl3.Invalidate();
            }

            //zedGraphControl5.GraphPane входной сигнал - окно
            {
                GraphPane plane = zedGraphControl5.GraphPane;
                plane.Title.Text = "Входной сигнал минус окно " + len.ToString() + " выборок";
                plane.YAxis.Title.Text = "Напряжение";
                plane.XAxis.Title.Text = "Выброка";

                plane.CurveList.Clear();
                PointPairList pointPairs = new PointPairList();

                for (int i = 0; i < conver.Filt_Data.Length; i++)
                {
                    pointPairs.Add(i, conver.Filt_Data[i]);
                }

                LineItem myCuve = plane.AddCurve("Входной сигнал минус окно", pointPairs, Color.Black, SymbolType.None);

                plane.XAxis.Scale.Min = 0;
                plane.XAxis.Scale.Max = conver.Filt_Data.Length;

                zedGraphControl5.AxisChange();
                zedGraphControl5.Invalidate();
            }


            if (conver.Period_data == null || conver.Period_data.Count == 0)
                return;
            //рисуем кореляционную кривую!
            {
                GraphPane plane = zedGraphControl2.GraphPane;

                plane.Title.Text = "Средне-квадратичное отклонение к размеру окна.";
                plane.YAxis.Title.Text = "Величина отклонения";
                plane.XAxis.Title.Text = "Размер окна";

                plane.CurveList.Clear();
                PointPairList pointPairs = new PointPairList();

                for (int i = 0; i < conver.Period_data.Count; i++)
                    pointPairs.Add(conver.Period_data[i].per, conver.Period_data[i].val);

                LineItem myCuve = plane.AddCurve("Средне-квадратичное отклонение", pointPairs, Color.Blue, SymbolType.None);
                plane.XAxis.Scale.Min = conver.Period_data[0].per;
                plane.XAxis.Scale.Max = conver.Period_data[conver.Period_data.Count - 1].per;

                //PointPairList Point = new PointPairList();
                /*
                for (int i = 0; i < conver.Min_samples.Count - 1; i++) {
                    Point.Add(new PointPair(conver.Period_data[i].per, conver.Period_data[i].val));
                }
                */
                pointPairs = new PointPairList();

                for (int i = 0; i < conver.Min_samples.Count; i++)
                    pointPairs.Add(conver.Min_samples[i].per, conver.Min_samples[i].val);

                LineItem myCuve_point = plane.AddCurve("", pointPairs, Color.Red, SymbolType.Circle);
                myCuve_point.Label.IsVisible = false;
                myCuve_point.Line.IsVisible = false;
                myCuve_point.Symbol.Fill.Type = FillType.Solid;
                myCuve_point.Symbol.Size = 7;

                pointPairs = new PointPairList();

                for (int i = 0; i < conver.Lock_Points.Count; i++)
                {
                    pointPairs = new PointPairList();
                    pointPairs.Add(conver.Lock_Points[i].Store.per, conver.Lock_Points[i].Store.val);

                    LineItem myCuve_point_lock = plane.AddCurve("", pointPairs, Color.Blue, SymbolType.Circle);
                    myCuve_point_lock.Label.IsVisible = false;
                    myCuve_point_lock.Line.IsVisible = false;
                    myCuve_point_lock.Symbol.Fill.Type = FillType.Solid;
                    myCuve_point_lock.Symbol.Size = (float)( 20 * conver.Lock_Points[i].GetStrength());
                }

                //plane.YAxis.Scale.Min = 0.0;
                //plane.YAxis.Scale.Max = 3.0;


                zedGraphControl2.AxisChange();
                zedGraphControl2.Invalidate();

            }

            label7.Text = "Статус: " + conver.State.ToString();
            if (double.IsNaN(conver.PredPeriod) == true)
            {
                label8.Text = "Периуд: NaN";
            }
            else
            {
                label8.Text = "Периуд: " + conver.PredPeriod.ToString();
                if (checkBox1.Checked == true)
                    numericUpDown1.Value = (decimal)conver.PredPeriod;
            }


        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            FileStream Raw_file = new FileStream("raw_data.txt", FileMode.Open, FileAccess.Read, FileShare.Read);
            //FileStream Raw_file = new FileStream("raw_data.csv", FileMode.Open, FileAccess.Read, FileShare.Read);
            if (Raw_file.CanRead == false)
                MessageBox.Show("Рядом с *.exe должен лежать файл raw_data.csv \r\n Доступный для чтения");
            else
            {
                raw_Data = new Raw_data(Raw_file);
                conver = new Conver(4000);
                GUI_Raw_data_Update(raw_Data);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

            //conver.AddSampl(raw_Data.Get_new_sample());
            double raw_sample = raw_Data.Get_new_sample(comboBox1.SelectedIndex);
            conver.AddSampl(raw_sample);
            int item_num = (conver.Count); 
            if (item_num > (int)numericUpDown1.Value)
                item_num = (int)numericUpDown1.Value;
            if (conver.State != Conver.eSyncState.NotSynced)
                conver.AddFiltSampl(raw_sample-conver.Item(conver.Count-item_num));
            conver.FindPeriod(300, 1500);

            GUI_Raw_data_Update(raw_Data);
            GUI_Conver_data_Update(conver);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (play == true)
                button1_Click(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            play = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            play = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
            
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            GUI_Conver_data_Update(conver);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            raw_Data.pos = (int)numericUpDown3.Value;
        }
    }

    public class Raw_data
    {
        //private Source_Read_CSV
        public List<double[]> data;
        public int pos;

        public double Get_new_sample()
        {
            if (pos < data.Count-1)
                pos++;
            else
                pos = 0;

            return data[pos][0];
        }
        public double Get_new_sample(int subarr)
        {
            if (pos < data.Count - 1)
                pos++;
            else
                pos = 0;

            return data[pos][subarr];
        }

        public Raw_data(FileStream data_source)
        {


            //Парсинг новых данных
            data = new List<double[]>();
            for (; data_source.Position < data_source.Length;)
            {
                //string time = string.Empty;
                string raw_val = string.Empty;
                Double[] val = new Double[0];

                //\r\n
                for (; data_source.Position < data_source.Length;)
                {
                    char new_char = Convert.ToChar(data_source.ReadByte());
                    if (new_char == '\r')
                    {
                        new_char = Convert.ToChar(data_source.ReadByte());
                        if (new_char == '\n')
                            break;
                    }
                    if (new_char == '\n')
                        break;
                    if (new_char == '\t')
                    {
                        val = val.Append(Convert.ToDouble(raw_val)).ToArray();
                        raw_val = string.Empty;
                    }
                    else
                        raw_val += new_char;

                }
                data.Add(val);
            }
            
            pos = 0;
        }
    }

    public class Conver
    {
        public class cStd_dev
        {
            public double per;
            public double val;

            //убрать
            public static cStd_dev min(cStd_dev s1, cStd_dev s2)
            {
                if (s1.val < s2.val)
                    return s1;
                else
                    return s2;
            }

            public static bool data_is_valid(double[] part_1, double[] part_2)
            {
                if (part_1.Length != part_2.Length)
                    return false;
                return true;
            }
            public static bool data_is_valid(ArraySegment<double> part_1, ArraySegment<double> part_2)
            {
                if (part_1.Count != part_2.Count)
                    return false;
                return true;
            }

            public cStd_dev(ArraySegment<double> part_1, ArraySegment<double> part_2)
            {
                int min_len = part_1.Count;
                if (min_len > part_2.Count)
                    min_len = part_2.Count;

                double x_disp = 0.0;

                for (int i = 0; i < min_len; i++)
                {
                    double prt_1 = part_1.Array[part_1.Offset + i];
                    double prt_2 = part_2.Array[part_2.Offset + i];


                    double x_delta = prt_1 - prt_2;
                    x_disp += (x_delta * x_delta);
                }
                //считам дисперсию

                //-1 потому что: Если бы эти данные были случайной выборкой из какой-то большой совокупности 
                //(например, оценки восьми случайно выбранных учеников большого города),
                //то в знаменателе формулы для вычисления дисперсии вместо n нужно было бы поставить n − 1:
                x_disp = x_disp / (min_len - 1);

                val = Math.Sqrt(x_disp);
                per = min_len;
            }

            public cStd_dev(double[] part_1, double[] part_2)
            {
                int min_len = part_1.Length;
                if (min_len > part_2.Length)
                    min_len = part_2.Length;

                double x_disp = 0.0;

                for (int i = 0; i < min_len; i++)
                {
                    double x_delta = part_1[i] - part_2[i];
                    x_disp += (x_delta * x_delta);
                }
                //считам дисперсию

                //-1 потому что: Если бы эти данные были случайной выборкой из какой-то большой совокупности 
                //(например, оценки восьми случайно выбранных учеников большого города),
                //то в знаменателе формулы для вычисления дисперсии вместо n нужно было бы поставить n − 1:
                x_disp = x_disp / (min_len - 1);

                val = Math.Sqrt(x_disp);
                per = min_len;
            }
            public cStd_dev()
            {
                per = 0;
                val = 0;
            }
        };

        public class cFinalPoint
        {

            public cStd_dev Store;

            //Антидребезг
            private int Disbounce_Cnt;
            private int Disbounce_Cnt_Max;

            public cFinalPoint(cStd_dev arg, int max_cnt)
            {
                Store = arg;
                Disbounce_Cnt_Max = max_cnt;
                Normalase_val = double.NaN;
            }
            public cStd_dev AddRawPoint(cStd_dev raw_val, bool store_raw_is_same)
            {
                if (store_raw_is_same == false)
                {
                    if (Disbounce_Cnt > Disbounce_Cnt_Max)
                    {
                        Store = raw_val;
                        Disbounce_Cnt = 0;
                    }
                    else
                        Disbounce_Cnt++;
                }
                else
                    Disbounce_Cnt = 0;
                return Store;
            }
            public double GetStrength()
            {
                return (1 - (double)Disbounce_Cnt / (double)Disbounce_Cnt_Max);
            }

            //Сложная обработка
            public double Normalase_val;
            public void Normalase(double stdev_max, double stdev_min)
            {
                double procent = (stdev_max - stdev_min) / 100;
                Normalase_val = (Store.val - stdev_min) / procent;
            }


            public static eSyncState GetPointState(List<cFinalPoint> cFinalPoints, out double period_pred)
            {
                period_pred = double.NaN;


                //Вышибала
                if (cFinalPoints.Count == 0)
                    return eSyncState.NotSynced;
                for (int i = 0; i < cFinalPoints.Count; i++)
                {
                    if  (double.IsNaN(cFinalPoints[i].Normalase_val) == true)
                    {
                        return eSyncState.NotSynced;
                    }
                }

                //Анализ
                List<cStd_dev> item_in_range = new List<cStd_dev>();
                List <cStd_dev> item_corelated = new List<cStd_dev>();


                for (int i = 0; i < cFinalPoints.Count; i++)
                    if (cFinalPoints[i].Normalase_val < 20)
                        item_in_range.Add(cFinalPoints[i].Store);

                if (item_in_range.Count == 1)
                {
                    period_pred = item_in_range[0].per;
                    return eSyncState.Unstable;
                }

                if (item_in_range.Count >= 2)
                {
                    double ratio = item_in_range[1].per / item_in_range[0].per;
                    if (ratio > 1.9 && ratio < 2.1)
                    {
                        period_pred = item_in_range[0].per;
                        return eSyncState.Stable;
                    }
                    else
                    {
                        period_pred = item_in_range[0].per;
                        return eSyncState.Unstable;
                    }
                }
                return eSyncState.NotSynced;
            }
        };

        public enum eSyncState
        {
            NotSynced, //Не готов
            Unstable, //Не стабильно
            Stable, //Не стабильно
        }

        private double[] Data;
        public double[] Filt_Data;
        public List<cStd_dev> Period_data;
        //public List<cStd_dev> Period_Median_data;//сортировка
        public List<cStd_dev> Min_samples;
        public List<cFinalPoint> Lock_Points;
        public bool Synced;
        public bool Fulled;
        public int  MaxLen;
        public eSyncState State;
        public double PredPeriod;

        public int Count { get { return Data.Length; } }
        public double Item (int index)
        {
            return Data[index];
        }

        private static void ShiftLeft<T>(List<T> lst, int shifts)
        {
            for (int i = shifts; i < lst.Count; i++)
            {
                lst[i - shifts] = lst[i];
            }

            for (int i = lst.Count - shifts; i < lst.Count; i++)
            {
                lst[i] = default(T);
            }
        }
        private static void ShiftRight<T>(List<T> lst, int shifts)
        {
            for (int i = lst.Count - shifts - 1; i >= 0; i--)
            {
                lst[i + shifts] = lst[i];
            }

            for (int i = 0; i < shifts; i++)
            {
                lst[i] = default(T);
            }
        }
        private static void ShiftLeft<T>(T[] arr, int shifts)
        {
            Array.Copy(arr, shifts, arr, 0, arr.Length - shifts);
            Array.Clear(arr, arr.Length - shifts, shifts);
        }
        private static void ShiftRight<T>(T[] arr, int shifts)
        {
            Array.Copy(arr, 0, arr, shifts, arr.Length - shifts);
            Array.Clear(arr, 0, shifts);
        }

        
        public void AddSampl(double raw)
        {
            if (Data.Length == MaxLen)
            {
                ShiftLeft(Data, 1);
                Data[Data.Length - 1] = raw;
                Fulled = true;
            }
            else
            {
                Data=Data.Append(raw).ToArray();
                Fulled = false;
            }
        }
        public void AddFiltSampl(double raw)
        {//это убираем из класса оно не неужно
            if (Filt_Data.Length == MaxLen)
            {
                ShiftLeft(Filt_Data, 1);
                Filt_Data[Filt_Data.Length - 1] = raw;
                Fulled = true;
            }
            else
            {
                Filt_Data = Filt_Data.Append(raw).ToArray();
                Fulled = false;
            }
        }
        public List<cStd_dev> FindPeriod(int min, int max)
        {
            if (max > MaxLen / 2)
                max = MaxLen / 2;

            if (max > Data.Length / 2)
                max = Data.Length / 2;

            if (min > Data.Length / 2)
                return null;

            List<cStd_dev> result = new List<cStd_dev>(max/2);

            //тут в цикле делаем расчёт среднеквардратичных отклонений для все диапазонов окон.
            {
                int pos = min;
                for (; pos < max; pos++){
                    //тут формируем лист среднеквадратичных отклонений для двух окон
                    ArraySegment<double> seg_1 = new ArraySegment<double>(Data, Data.Length - pos, pos);
                    ArraySegment<double> seg_2 = new ArraySegment<double>(Data, Data.Length - pos*2, pos);

                    if (cStd_dev.data_is_valid(seg_1, seg_2) == true)
                    {
                        cStd_dev sample = new cStd_dev(seg_1, seg_2);
                        result.Add(sample);
                    }


                }
            }

            Period_data = result;


            Min_samples.Clear();
            List<cStd_dev> Min_samples_prepair = new List<cStd_dev>();
            
            //Ишем нижние горбы
            for (int i = 0; i < Period_data.Count; i++)
            {
                cStd_dev Item1, Item2;
                double dev_prv = 0;
                for (int ii = i; ii < Period_data.Count - 1; ii++)
                {
                    Item1 = Period_data[ii];
                    Item2 = Period_data[ii + 1];
                    double dev = Item1.val - Item2.val;

                    if ((dev_prv > 0) && (dev < 0))
                    {
                        Min_samples_prepair.Add(Period_data[ii]);
                        i = ii;
                    }
                    dev_prv = dev;
                }
            }

            //ОСтавляем 3 самые минималные точки.
            for (int i = 0; (i < 3 && Min_samples_prepair.Count > 0); i++)
            {
                double val = double.MaxValue;
                int item = 0;
                for (int ii = 0; ii < Min_samples_prepair.Count; ii++)
                {
                    if (Min_samples_prepair[ii].val < val)
                    {
                        val = Min_samples_prepair[ii].val;
                        item = ii;
                    }
                    
                }
                Min_samples.Add(Min_samples_prepair[item]);
                Min_samples_prepair.RemoveAt(item);
            }

            //Делаем антидребезг точек(захват)
            {
                if (Lock_Points.Count > Min_samples.Count)
                    Lock_Points.Clear();
                for (int ii = 0; Lock_Points.Count < Min_samples.Count; ii++)
                    Lock_Points.Add(new cFinalPoint(Min_samples[ii], 100));

                for (int i = 0; (i < Lock_Points.Count); i++)
                {
                    double per = Lock_Points[i].Store.per;

                    for (int ii = 0; (ii < Min_samples.Count); ii++)
                    {   
                        double delta = Math.Abs(per - Min_samples[ii].per);

                        if (delta < 3)
                        {
                            Lock_Points[i].AddRawPoint(Min_samples[i], true);
                            goto next_point;
                        }
                    }

                    Lock_Points[i].AddRawPoint(Min_samples[i], false);
                next_point:;
                }
            }

            //поиск максмума минимума маштабирование результата
            {
                double stDev_max = double.MinValue;
                double stDev_min = double.MaxValue;
                for (int i = 0; i < Period_data.Count; i++)
                {
                    if (Period_data[i].val > stDev_max)
                        stDev_max = Period_data[i].val;
                    if (Period_data[i].val < stDev_min)
                        stDev_min = Period_data[i].val;
                }
                for (int i = 0; i < Lock_Points.Count; i++)
                {
                    if (Lock_Points[i].Store.val < stDev_min)
                        stDev_min = Lock_Points[i].Store.val;
                }

                for (int i = 0; i < Lock_Points.Count; i++)
                {
                    Lock_Points[i].Normalase(stDev_max, stDev_min);
                }

                State = cFinalPoint.GetPointState(Lock_Points, out PredPeriod);
            }
            return result;
        }


        public Conver(int maxlen)
        {
            Data = new double[0];
            Filt_Data = new double[0];
            Min_samples = new List<cStd_dev>();
            Lock_Points = new List<cFinalPoint>();
            Synced = false;
            Fulled = false;
            MaxLen = maxlen;
        }
    }

}
